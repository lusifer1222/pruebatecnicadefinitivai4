#language:es
Característica: Yo como usuario de trenes
Quiero Reservar un viaje en tren
Para viajar con mi familia

Antecedentes:
Dado estoy en la pagina de perurail

  Esquema del escenario: Reservar un viaje para un nino
    Cuando lleno el paso uno
      | cantidadAdultos   | cantidadNinos   |
      | <cantidadAdultos> | <cantidadNinos> |
    Entonces No permite pasar
      | cantidadAdultos   | cantidadNinos   |
      | <cantidadAdultos> | <cantidadNinos> |
    Ejemplos:
      | cantidadAdultos | cantidadNinos |
      | 0               | 1             |



