package co.com.certificacion.perurail.stepsdefinitions;

import co.com.perurali.model.DatosEntrada;
import co.com.perurali.question.CompararPrecioCarritoEleccion;
import co.com.perurali.task.LlenarPaso1;
import co.com.perurali.task.LlenarPaso2;
import co.com.perurali.task.LlenarPaso3;
import co.com.perurali.task.LlenarPaso4;
import co.com.perurali.utils.constants.Constantes;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static org.hamcrest.Matchers.equalTo;


public class ReservaExitosaStepDefinition {
    @Before
    public void setUp(){
        setTheStage(new OnlineCast());
        theActorCalled(Constantes.NOMBRE_ACTOR.getConstante());
    }
    @Dado("estoy en la pagina de perurail")
    public void estoyEnLaPaginaDePerurail() {
        theActorInTheSpotlight().wasAbleTo(Open.url(Constantes.URL_PERURALI.getConstante()));
    }

    @Cuando("lleno el formulario con los datos")
    public void llenoElFormularioConLosDatos(List<DatosEntrada> datos) {
        theActorInTheSpotlight().attemptsTo(LlenarPaso1.ir(datos.get(0)));
        theActorInTheSpotlight().attemptsTo(LlenarPaso2.ir());
        theActorInTheSpotlight().attemptsTo(LlenarPaso3.ir());
        theActorInTheSpotlight().attemptsTo(LlenarPaso4.ir());
    }
    @Entonces("el precio del ticket en el ultimo paso es correcto")
    public void elPrecioDelTicketEnElUltimoPasoEsCorrecto() {
        theActorInTheSpotlight().should(seeThat(CompararPrecioCarritoEleccion.con(), equalTo("USD 39.00")));
    }
}
