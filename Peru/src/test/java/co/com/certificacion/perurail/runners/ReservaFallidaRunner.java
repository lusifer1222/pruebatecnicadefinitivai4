package co.com.certificacion.perurail.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/ReservaFallida.feature",
        glue = "co.com.certificacion.perurail.stepsdefinitions",
        plugin = {"pretty"},
        snippets = CucumberOptions.SnippetType.CAMELCASE
        //dryRun = true
)

public class ReservaFallidaRunner {
}
