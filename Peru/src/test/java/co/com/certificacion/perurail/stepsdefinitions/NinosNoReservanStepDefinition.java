package co.com.certificacion.perurail.stepsdefinitions;

import co.com.perurali.model.DatosEntrada;
import co.com.perurali.question.CompararNumeroAdultos;
import co.com.perurali.task.LlenarPaso1;
import co.com.perurali.utils.constants.Constantes;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class NinosNoReservanStepDefinition {
    @Before
    public void setUp(){
        setTheStage(new OnlineCast());
        theActorCalled(Constantes.NOMBRE_ACTOR.getConstante());
    }
    @Cuando("lleno el paso uno")
    public void llenoElPasoUno(List<DatosEntrada> datos) {
        theActorInTheSpotlight().attemptsTo(LlenarPaso1.ir(datos.get(0)));
    }

    @Entonces("No permite pasar")
    public void noPermitePasar(List<DatosEntrada> datos) {
        theActorInTheSpotlight().should(seeThat(CompararNumeroAdultos.con(),equalTo(datos.get(0).getCantidadAdultos())));
    }
}
