package co.com.perurali.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class Paso1Page {

    public static final Target BTN_ONEWAY = Target.the("Un solo viaje").locatedBy("//*[@id='radioset']/div[2]/label/span");
    public static final Target BTN_ROUNDTRIP = Target.the("Viaje redondo").located(By.id("roundtrip"));
    public static final Target LIST_DESTINO = Target.the("Destino").located(By.id("destinoSelect"));
    public static final Target LIST_RUTA = Target.the("Ruta").located(By.id("rutaSelect"));
    public static final Target LIST_CANT_ADULT_NINO = Target.the("Cantidad de Adultos o niños").located(By.id("countParentsChildren"));
    public static final Target BTN_DIS_CANT_ADULT = Target.the("Disminuir Cantidad de Adultos").located(By.id("adultsDism"));
    public static final Target BTN_AUM_CANT_ADULT = Target.the("Aumentar Cantidad de Adultos").located(By.id("adultsAume"));
    public static final Target BTN_AUM_CANT_NINO = Target.the("Aumentar Cantidad de niños").located(By.id("childrenAume"));
    public static final Target BTN_ENCUENTRA_TIKCETS = Target.the("MENU").located(By.id("btn_search"));

}
