package co.com.perurali.userinterface;

import net.serenitybdd.screenplay.targets.Target;

import java.time.Duration;

public class Paso3Page {
    public static final Target TXT_NAME_ADULT = Target.the("Nombre de adulto").locatedBy("//*[@id='formPasajero1-nomPasajero']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target TXT_APELLIDO_ADULT = Target.the("APELLIDO de adulto").locatedBy("//*[@id='formPasajero1-apePasajero']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target BTN_CALENDARIO = Target.the("APELLIDO de adulto").locatedBy("//*[@id='formPasajero1-fecNacimiento']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target LST_MES = Target.the("VIAJAR MARTES").locatedBy("//*[@id='calendario_mes']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target LST_ANIO = Target.the("VIAJAR MARTES").locatedBy("//*[@id='calendario_anio']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target LST_DIA = Target.the("VIAJAR MARTES").locatedBy("//*[@id='tlb_calendario']/tbody/tr[4]/td[3]").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target LST_PAISES = Target.the("DIA SIGUIENTE").locatedBy("//*[@id='formPasajero1-idPais']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target LST_TIPO_DOC = Target.the("DIA SIGUIENTE").locatedBy("//*[@id='formPasajero1-idDocumentoIdentidad']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target TXT_NUM_DOC = Target.the("DIA SIGUIENTE").locatedBy("//*[@id='formPasajero1-numDocumentoIdentidad']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target LST_SEXO = Target.the("DIA SIGUIENTE").locatedBy("//*[@id='formPasajero1-idSexo']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target TXT_NUM_TEL = Target.the("TICKETE QUE TIENE PROMOCION").locatedBy("//*[@id='formPasajero1-numTelefono']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target TXT_CORREO = Target.the("CONTINIUAR").locatedBy("//*[@id='formPasajero1-desEmail']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target TXT_CORREO_CONF= Target.the("CONTINIUAR").locatedBy("//*[@id='formPasajero1-desEmailConfirmacion']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target BTN_RECIBIR_PROMO= Target.the("CONTINIUAR").locatedBy("//*[@id='']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target BTN_ENVIAR= Target.the("CONTINIUAR").locatedBy("//*[@id='enviarPago']").waitingForNoMoreThan(Duration.ofSeconds(3) );


}
