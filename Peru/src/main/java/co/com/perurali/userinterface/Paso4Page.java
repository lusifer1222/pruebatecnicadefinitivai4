package co.com.perurali.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import java.time.Duration;

public class Paso4Page {
    public static final Target BTN_FORMAPAGO = Target.the("Pago internacional").located(By.id("pago_internacional"));
    public static final Target BTN_ACEPATTER1 = Target.the("TERMINO 1").located(By.id("terms"));
    public static final Target BTN_ACEPATTER2 = Target.the("TERMINO 2").located(By.id("termsBimodal"));
    public static final Target BTN_ACEPATTER3 = Target.the("TERMINO 3").located(By.id("termsPromo"));
    public static final Target BTN_PAGAR = Target.the("BOTON PAGAR").located(By.id("text_form_submit_payment"));

}
