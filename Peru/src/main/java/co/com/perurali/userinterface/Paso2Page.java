package co.com.perurali.userinterface;

import net.serenitybdd.screenplay.targets.Target;

import java.time.Duration;

public class Paso2Page {
    public static final Target TXT_PRECIO_TOTAL_CARRITO = Target.the("Precio total carrito").locatedBy("//*[@class='pasajeros']/div[2]").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target BTN_DIA_SIGUIENTE = Target.the("DIA SIGUIENTE").locatedBy("//*[@id='viajeIdaCapaBimodal']/div/div[3]/div[2]/div[1]/div/div[4]").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target BTN_TIENE_PROMO = Target.the("TICKETE QUE TIENE PROMOCION").locatedBy("//*[@tienepromocion='1'][1]").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target BTN_CONTINUAR = Target.the("CONTINIUAR").locatedBy("//*[@class='btn  btn-continuar']").waitingForNoMoreThan(Duration.ofSeconds(3) );
    public static final Target LIS_CANT_ADULT = Target.the("Cantidad Adultos").locatedBy("//*[@id='numCupoAdulto']").waitingForNoMoreThan(Duration.ofSeconds(3) );

}
