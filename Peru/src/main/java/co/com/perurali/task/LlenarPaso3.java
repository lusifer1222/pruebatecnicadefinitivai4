package co.com.perurali.task;

import co.com.perurali.interation.Wait;
import co.com.perurali.userinterface.Paso3Page;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LlenarPaso3 implements Task {


    public LlenarPaso3() {
        super();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue("Mariana").into(Paso3Page.TXT_NAME_ADULT));
        actor.attemptsTo(Enter.theValue("Rodriguez").into(Paso3Page.TXT_APELLIDO_ADULT));
        actor.attemptsTo(Click.on(Paso3Page.BTN_CALENDARIO));
        actor.attemptsTo(SelectFromOptions.byValue("5").from(Paso3Page.LST_MES));
        actor.attemptsTo(SelectFromOptions.byValue("2001").from(Paso3Page.LST_ANIO));
        actor.attemptsTo(Click.on(Paso3Page.LST_DIA));
        actor.attemptsTo(SelectFromOptions.byValue("COL").from(Paso3Page.LST_PAISES));
        actor.attemptsTo(SelectFromOptions.byValue("DNI").from(Paso3Page.LST_TIPO_DOC));
        actor.attemptsTo(Enter.theValue("12345678").into(Paso3Page.TXT_NUM_DOC));
        actor.attemptsTo(SelectFromOptions.byValue("F").from(Paso3Page.LST_SEXO));
        actor.attemptsTo(Enter.theValue("+573207519395").into(Paso3Page.TXT_NUM_TEL));
        actor.attemptsTo(Enter.theValue("luisa@luisa.com.co").into(Paso3Page.TXT_CORREO));
        actor.attemptsTo(Enter.theValue("luisa@luisa.com.co").into(Paso3Page.TXT_CORREO_CONF));
        actor.attemptsTo(Click.on(Paso3Page.BTN_ENVIAR));
        actor.attemptsTo( Wait.waitClass(9000));
    }

    public static LlenarPaso3 ir() {
        return instrumented(LlenarPaso3.class);
    }
}
