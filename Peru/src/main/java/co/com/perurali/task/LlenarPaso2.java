package co.com.perurali.task;

import co.com.perurali.userinterface.Paso2Page;
import co.com.perurali.utils.constants.Constantes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LlenarPaso2 implements Task {

    public LlenarPaso2() {
        super();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(Paso2Page.BTN_DIA_SIGUIENTE));
        actor.attemptsTo(Click.on(Paso2Page.BTN_TIENE_PROMO));
        actor.attemptsTo(Click.on(Paso2Page.BTN_CONTINUAR));
    }

    public static LlenarPaso2 ir() {
        return instrumented(LlenarPaso2.class);
    }
}
