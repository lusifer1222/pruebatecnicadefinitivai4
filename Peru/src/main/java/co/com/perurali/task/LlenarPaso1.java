package co.com.perurali.task;

import co.com.perurali.model.DatosEntrada;
import co.com.perurali.userinterface.Paso1Page;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;


import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LlenarPaso1 implements Task {
    private DatosEntrada datos;


    public LlenarPaso1(DatosEntrada datos) {
        this.datos=datos;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(Paso1Page.BTN_ONEWAY));
        actor.attemptsTo(SelectFromOptions.byValue("1").from(Paso1Page.LIST_DESTINO));
        actor.attemptsTo(SelectFromOptions.byValue("12").from(Paso1Page.LIST_RUTA));
        actor.attemptsTo(Click.on(Paso1Page.LIST_CANT_ADULT_NINO));
        actor.attemptsTo(Click.on(Paso1Page.BTN_DIS_CANT_ADULT),
                Click.on(Paso1Page.BTN_DIS_CANT_ADULT));
        for (int i = 0; i < new Integer(datos.getCantidadAdultos()); i++) {
            actor.attemptsTo(Click.on(Paso1Page.BTN_AUM_CANT_ADULT));
        }
        for (int i = 0; i < new Integer(datos.getCantidadNinos()); i++) {
            actor.attemptsTo(Click.on(Paso1Page.BTN_AUM_CANT_NINO));
        }
        actor.attemptsTo(Click.on(Paso1Page.BTN_ENCUENTRA_TIKCETS));
    }

    public static LlenarPaso1 ir(DatosEntrada datos) {
        return instrumented(LlenarPaso1.class,datos);
    }
}
