package co.com.perurali.task;

import co.com.perurali.interation.Wait;
import co.com.perurali.userinterface.Paso3Page;
import co.com.perurali.userinterface.Paso4Page;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LlenarPaso4 implements Task {


    public LlenarPaso4() {
        super();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(Paso4Page.BTN_FORMAPAGO));
        actor.attemptsTo(Click.on(Paso4Page.BTN_ACEPATTER1));
        actor.attemptsTo(Click.on(Paso4Page.BTN_ACEPATTER2));
        actor.attemptsTo(Click.on(Paso4Page.BTN_ACEPATTER3));
        actor.attemptsTo(Click.on(Paso4Page.BTN_PAGAR));
        actor.attemptsTo( Wait.waitClass(9000));

    }

    public static LlenarPaso4 ir() {
        return instrumented(LlenarPaso4.class);
    }
}
