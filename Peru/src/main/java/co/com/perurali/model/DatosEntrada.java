package co.com.perurali.model;

public class DatosEntrada {


    private String cantidadAdultos;
    private String cantidadNinos;
    private String precio;

    public DatosEntrada() {
    }

    public String getCantidadAdultos() {
        return cantidadAdultos;
    }

    public void setCantidadAdultos(String cantidadAdultos) {
        this.cantidadAdultos = cantidadAdultos;
    }

    public String getCantidadNinos() {
        return cantidadNinos;
    }

    public void setCantidadNinos(String cantidadNinos) {
        this.cantidadNinos = cantidadNinos;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
