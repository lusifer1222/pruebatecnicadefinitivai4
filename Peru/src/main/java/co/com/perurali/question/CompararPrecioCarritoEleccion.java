package co.com.perurali.question;

import co.com.perurali.userinterface.Paso2Page;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class CompararPrecioCarritoEleccion implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        String preciocarro = Text.of(Paso2Page.TXT_PRECIO_TOTAL_CARRITO).viewedBy(actor).value();
        return preciocarro;
    }
    public static CompararPrecioCarritoEleccion con(){return new CompararPrecioCarritoEleccion();}
}
