package co.com.perurali.question;

import co.com.perurali.userinterface.Paso2Page;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class CompararNumeroAdultos implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Paso2Page.LIS_CANT_ADULT.resolveFor(actor).getSelectedValue();
    }
    public static CompararNumeroAdultos con(){return new CompararNumeroAdultos();}
}
