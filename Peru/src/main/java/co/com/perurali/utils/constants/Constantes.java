package co.com.perurali.utils.constants;

public enum Constantes {
    URL_PERURALI("https://www.perurail.com"),
    NOMBRE_ACTOR("Luisa"),
    PRECIO_CARRITO_COMPRAS("Precio_carrito");

    private String constante;
    Constantes(String constante) {
        this.constante = constante;
    }

    public String getConstante() {
        return constante;
    }
}
