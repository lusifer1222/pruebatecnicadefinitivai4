#language:es
Característica: Yo como usuario de trenes
  Quiero Reservar un viaje en tren
  Para viajar con mi familia

  Antecedentes:
    Dado estoy en la pagina de perurail

  Esquema del escenario: Reservar un viaje a cusco para un adulto
    Cuando lleno el formulario con los datos
      | cantidadAdultos   | cantidadNinos   | precio   |
      | <cantidadAdultos> | <cantidadNinos> | <precio> |
    Entonces el precio del ticket en el ultimo paso es correcto
    Ejemplos:
      | cantidadAdultos | cantidadNinos | precio  |
      | 1               | 0             | USD 390 |
